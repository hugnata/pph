# Projet Personnel en Humanités

Projet effectué dans le cadre des humanités à l'INSA de Lyon. Il s'agit d'une reflexion sur les outils et processus à mettre en place pour une passation dans le cadre associatif. 

Le rapport a été édité sous Overleaf, mais je laisse les sources disponibles sur ce repo.

## Documents

Ce dépot propose aussi certains documents décrits dans le rapport. Ils sont disponibles dans le répertoire documents


## Grille d'évaluation des outils

Vous retrouverez dans le dossier divers une grille d'évaluation de l'impact des outils/processus. 
Les différents critères d'évaluations sont décrits ci dessous:


#### Utilité du document/processus: 

Sur une échelle de 0 à 10, à combien estimez vous l'utilité du document/processus ? 
Si vous arriviez sur un projet informatique, à quel point aimeriez vous avoir ce document/processus ?

####  La structure de l'information: 

Sur une échelle de 0 à 10: L'information est-elle structurée, est-t-il facile d'identifier des axes, de retrouver rapidement une information ?

#### Convivialité du support: 

Sur une échelle de 0 à 10, à quel point le support, processus,... est-il agréable à utiliser ? 


#### Investissement: 

Combien d'heure seriez prêt à passer pour rédiger ce document/organiser ce processus ?
















